(load "php-mode")
(load "markdown-mode")
(load "html-helper-mode")
(autoload 'js2-mode "js2-mode" nil t)

; Autocomplete

(require 'auto-complete-config)
(add-to-list 'ac-dictionary-directories "~/.emacs.d//ac-dict")
(ac-config-default)

; Set Modes

(setq auto-mode-alist (cons '("\\.ctp$" . html-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.php$" . php-mode) auto-mode-alist))
(add-to-list 'auto-mode-alist '("\\.mdown$" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.markdown$" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.md$" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.js$" . js2-mode))

; Config
(tool-bar-mode -1)
(setq html-helper-basic-offset 4)
(delete-selection-mode 1)
(setq tab-width 4)
(setq indent-line-function 'insert-tab)
(setq html-indent-level 4)
(setq default-tab-width tab-width)
(setq php-mode-force-pear t)
(add-hook 'before-save-hook 'delete-trailing-whitespace)
; (global-set-key (kbd "RET") 'newline-and-indent)


	
(add-hook 'sgml-mode-hook
	(lambda ()
	;; Default indentation to 2, but let SGML mode guess, too.
	(set (make-local-variable 'sgml-basic-offset) 4)
	(sgml-guess-indent)))

(add-hook 'php-mode-hook (lambda ()
	(setq indent-tabs-mode t)
	(setq tab-width 4)
	(setq c-basic-indent 4)
	(defun ywb-php-lineup-arglist-intro (langelem)
      (save-excursion
        (goto-char (cdr langelem))
        (vector (+ (current-column) c-basic-offset))))
    (defun ywb-php-lineup-arglist-close (langelem)
      (save-excursion
        (goto-char (cdr langelem))
        (vector (current-column))))
    (c-set-offset 'arglist-intro 'ywb-php-lineup-arglist-intro)
    (c-set-offset 'arglist-close 'ywb-php-lineup-arglist-close)))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (wombat)))
 '(tooltip-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )


;; Rectangle Selection

(require 'rect-mark)
(global-set-key (kbd "C-x r C-SPC") 'rm-set-mark)
(global-set-key (kbd "C-w")  
  '(lambda(b e) (interactive "r") 
     (if rm-mark-active 
       (rm-kill-region b e) (kill-region b e))))
(global-set-key (kbd "M-w")  
  '(lambda(b e) (interactive "r") 
     (if rm-mark-active 
       (rm-kill-ring-save b e) (kill-ring-save b e))))
(global-set-key (kbd "C-x C-x")  
  '(lambda(&optional p) (interactive "p") 
     (if rm-mark-active 
       (rm-exchange-point-and-mark p) (exchange-point-and-mark p))))
(put 'upcase-region 'disabled nil)
